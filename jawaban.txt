1. membuat database
create databases myshop;

2. membuat tabel
table users
MariaDB [myshop]> create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

table categories
MariaDB [myshop]> create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

table items
MariaDB [myshop]> create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

3. memasukan data ke tabel
table users
MariaDB [myshop]> insert into users(name, email, password)
    -> values("john doe", "john@doe.com", "john123");
MariaDB [myshop]> insert into users(name, email, password)
    -> values("jane doe", "jane@doe.com", "jenita123");


table categories
MariaDB [myshop]> insert into categories (name) values ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

table items
MariaDB [myshop]> insert into items(name, description, price, stock, categoey_id) values("samsung b5", "hape keren dari merk samsung", 4000000, 100, 1);

MariaDB [myshop]>insert into items(name, description, price, stock, category_id
) values("uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. mengambil data dari database
A. mengambil data users kecuali pasword
MariaDB [myshop]> select id, name, email from users;

B. mengambil data item
mengambil data di atas harga 1 juta
MariaDB [myshop]> select * from items where price>1000000;

mengambil data nama serupa sung
MariaDB [myshop]> select * from items where name like '%sung%';

C. menampilkan data item join 
MariaDB [myshop]> select items.name, items.description, items.price, items.stock
, items.category_id, categories.name as kategori from items inner join categorie
s on items.category_id = categories.id;

5. merubah data dari database
MariaDB [myshop]> update items set price=2500000 where id=1;
